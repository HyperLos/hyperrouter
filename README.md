# HyperRouter 
is a simple to use virtual router for Hyper-V Environments based on VyOS.

Microsoft includes Hyper-V as a free option with the Windows 10 Desktop Operating System. In order to create additional virtual networks you will need to enable the internet connection sharing service. This will only work if you have the following requirements:
1. Single additional private network
2. Do not have a VPN connection (Most VPN connections do not like the ICS service)


## Why do I need more than 1 virtual network?
When you are building different test or demo environments it is good to replicate how it may look in real production environments. HyperRouter will allow you to configure multiple networks so you can replicate configurations and isolate different environments.

## Why will you want to use HyperRouter? 
HyperRouter will allow you to configure a fully routed network with up to 9 internal separated networks with routes to the internet without intefering with your VPN client.


## Disclaimer: 
This is provided as it is. This is intended for test and dev environment so you can replicate your production environment.
